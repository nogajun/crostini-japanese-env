# Script to install Japanese environment on Linux(crostini)

This is a script to set up a Japanese environment and install LibreOffice on a Linux (crostini) on Cromebook.

Warning: **This script will destroy your existing Linux environment and should be used immediately after enabling Linux**

## How to use

1. Download the script([setup-crostini.sh](https://raw.githubusercontent.com/libreoffice-ja/setup-crostini/main/setup-crostini.sh)) to your chromebook.
2. Copy and paste the script from My Files/Downloads into a My Files/Linux file.
3. Launch a terminal and start it with the command below.

```console
bash ./setup-crostini.sh
```

4. Stop the Linux container by right-clicking the terminal icon and selecting "Shut down Linux". After a while, start the terminal.
5. Launch LibreOffice icon from launcher.

# ChromebookのLinux(crostini)に日本語環境とLibreOfficeをインストールするスクリプト

これは、ChromebookのLinux(crostini)に日本語環境の設定をして、LibreOfficeをインストールするためのスクリプトです。

警告: **このスクリプトは既存のLinux環境を破壊します。Linux機能を有効にした直後にご利用ください**

## 使い方

1. [setup-crostini.sh](https://raw.githubusercontent.com/libreoffice-ja/setup-crostini/main/setup-crostini.sh)スクリプトをChromebookにダウンロードします。
2. マイファイル/ダウンロードにあるスクリプトをコピーして、マイファイル/Linuxファイルに貼り付けます。
3. ターミナルを起動して、以下のコマンドで起動します。

```console
bash ./setup-crostini.sh
```

4. ターミナルアイコンを右クリックして、「Linuxをシャットダウン」からLinuxコンテナーを停止します。
   しばらくしてから、ターミナルを起動します。
5. ランチャーのLinuxアプリの中に、LibreOfficeのアイコンがあるので起動します。

## よくある質問

* Q:「Linuxをシャットダウン」から停止して再起動しても、LibreOfficeに日本語入力ができません。
    * たまに起こります。 **Chromebook自体** を再起動すると直るのでChromeOSの問題のような気がします。
* Q: ibusを使いたいです。
    * インストール前にスクリプト冒頭の`IMF="fcitx"`をibusに変更すると多分インストールできるはず。
* Q: HiDPIな画面のChromebookですがフォントの大きさを調整したいです。
    * `~/.sommelierrc`にコメントで参考設定を書いてるので調整してみてください。
* Q: LibreOfficeのUIが文字化けしました。
    * Flatpakのフォントキャッシュを作り直してください。ターミナルから `flatpak run --command=fc-cache org.libreoffice.LibreOffice -f -v`を実行します。
* Q: Linuxアプリにノイズが走ります。
    * 私も原因がわかりません。ChromeOSのせいだと思います。
* Q: Chromebookのフォントが/mnt/chromeos/fontsから利用できるのに、なんでわざわざフォントをインストールするんですか？
    * Flatpakアプリからフォントのパスが見えないからです。
* Q: なんでFlatpakのLibreOfficeを使ってるんですか？deb/Snap/AppImageを使わないんですか？
    * IntelとArm両方で使える最新のLibreOfficeがFlatpakしかなかったからです。スクリプトはCC0だし、ほかのパッケージを使いたいなら自分でインストールできるので[好きにしてください](https://www.youtube.com/watch?v=jcVstFIVJgg)。
* Q: 対応はBusterだけですか？[Bullseye](https://www.debian.org/releases/bullseye/)になっても対応しますか？
    * おそらくBullseyeでもそのままで動くはず。でも、自分が使っているので問題あれば直すかも。
* Q: ユーザーsystemdの設定から環境変数を設定しても反映されません
    * ChromeOSのせいだと思いますが、なぜか教えてほしいです。
* Q: わかんないけど動きません
    * 私もわかりません。とりあえず、Chromebookを再起動することと、もう少しわかる情報をください。

## Author and license

* Jun Nogata <nogajun+gitlab@gmail.com>
* Public domain / CC0 <http://creativecommons.org/publicdomain/zero/1.0/deed.ja>
