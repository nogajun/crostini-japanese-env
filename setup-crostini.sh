#!/bin/sh
#
# Japanese configuration automatic setup script for chromebook linux container.
# Jun Nogata <nogajun+gitlab@gmail.com>
# https://gitlab.com/nogajun/crostini-japanese-env/-/raw/main/setup-crostini.sh
#
# These codes are licensed under CC0.
# http://creativecommons.org/publicdomain/zero/1.0/deed.ja
#
# How to use:
# 1. Download this script to your chromebook.
# 2. Copy and paste the script from My Files/Downloads into a My Files/Linux file.
# 3. Launch a terminal and start it with the command below.
#    $ bash ./setup-crostini.sh
#

set -e

USER_LOCALE="ja_JP.UTF-8"
IMF="fcitx5"
FONT="Noto Sans CJK JP 9"
PACKAGES="lv manpages-ja nkf fonts-hack fonts-mplus nano tmux dialog"

# ----

BACKEND="wayland,x11"
. /etc/os-release

add_backports(){
  SOURCES_LIST=/etc/apt/sources.list.d/cros.list
  if [ -z "$(grep backports ${SOURCES_LIST})" ]; then
    sudo sh -c "echo \"deb https://deb.debian.org/debian ${VERSION_CODENAME}-backports main\" > ${SOURCES_LIST}"
  fi
}

set_timezone(){
  sudo ln -sf "/usr/share/zoneinfo/${TZONE}" /etc/localtime
  sudo echo ${TZONE} > /etc/timezone
}

set_locales(){
  sudo sh -c "\
  sed -i -e \"s/^# \(${USER_LOCALE}\)/\1/g\" /etc/locale.gen; \
  echo \"LANG=${USER_LOCALE}\" > /etc/default/locale; \
  dpkg-reconfigure --frontend=noninteractive locales; \
  update-locale LANG=${USER_LOCALE}"
}

install_flatpak(){
  sudo apt update
  case "${VERSION_CODENAME}" in
    "buster"|"bullseye")
      sudo apt -y -t ${VERSION_CODENAME}-backports install flatpak
      sudo apt -y -t ${VERSION_CODENAME}-backports install gnome-software gnome-software-plugin-flatpak
      ;;
    *)
      sudo apt -y -t ${VERSION_CODENAME} install flatpak
      sudo apt -y -t ${VERSION_CODENAME} install gnome-software gnome-software-plugin-flatpak
      ;;
  esac
  flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  # [ ${DISPLAY} = "wayland-0" ] && flatpak --user override --env=DISPLAY=wayland-0
  # [ ${IMF} != "" ] && flatpak --user override --env=GTK_IM_MODULE=${IMF} --env=QT_IM_MODULE=${IMF} --env=GTK_IM_MODULE=${IMF} --env=XMODIFIERS=@im=${IMF}
  sudo systemctl restart flatpak-system-helper.service
}

setup_env(){
  CONF="/etc/profile.d/im-config_sommelier.sh"
  if [ -n "${SOMMELIER_VERSION}" ]; then
    sudo tee -a ${CONF} << EOL
# /etc/profile.d/im-config_sommelier.sh
#
# This sets the IM variables on Sommelier.

if [ -z "\${SOMMELIER_VERSION}" ]; then
    return
fi

if [ -r /etc/X11/Xsession.d/70im-config_launch ]; then
    . /etc/X11/Xsession.d/70im-config_launch
fi

export GDK_BACKEND=${BACKEND}
EOL
  fi
}

setup_inputmethod(){
  case "${IMF}" in
    "ibus")
      sudo apt install -y ibus ibus-mozc im-config
      echo "GDK_BACKEND=${BACKEND} /usr/bin/ibus-daemon -rdx" >> ${HOME}/.sommelierrc
      setup_env
      ;;
    "fcitx")
      sudo apt install -y fcitx fcitx-mozc im-config
      echo "GDK_BACKEND=${BACKEND} /usr/bin/fcitx-autostart" >> ${HOME}/.sommelierrc
      mkdir -p ${HOME}/.config/fcitx/conf/
      cat << EOL > ${HOME}/.config/fcitx/config
[Hotkey]
TriggerKey=ALT_SPACE
EOL
      cat << EOL > ${HOME}/.config/fcitx/conf/fcitx-xim.config
[Xim]
UseOnTheSpotStyle=True
EOL
      setup_env
      ;;
    "fcitx5")
      sudo apt install -y fcitx5 fcitx5-mozc im-config
      echo "GDK_BACKEND=${BACKEND} /usr/bin/fcitx5 -d" >> ${HOME}/.sommelierrc
      mkdir -p ${HOME}/.config/fcitx5/conf/
      cat << EOL > ${HOME}/.config/fcitx5/config
[Hotkey]

[Hotkey/TriggerKeys]
0=Alt+space
EOL
      echo "UseOnTheSpot=True" >> ${HOME}/.config/fcitx5/conf/xim.conf
      IMF="fcitx"
      setup_env
      ;;
      *)
      ;;
  esac
}

user_fontconfig(){
  sudo tee -a /etc/fonts/local.conf << EOL
<?xml version="1.0"?><!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <dir>/usr/share/fonts</dir>
  <match target="font">
    <edit mode="assign" name="antialias"><bool>true</bool></edit>
    <edit mode="assign" name="embeddedbitmap"><bool>false</bool></edit>
    <edit mode="assign" name="hinting"><bool>false</bool></edit>
    <edit mode="assign" name="hintstyle"><const>hintnone</const></edit>
    <edit mode="assign" name="lcdfilter"><const>lcddefault</const></edit>
    <edit mode="assign" name="rgba"><const>rgb</const></edit>
  </match>
  <match>
    <edit mode="prepend" name="family">
      <string>Noto Sans</string>
    </edit>
  </match>
  <match target="pattern">
    <test name="lang"><string>ja</string></test>
    <test name="family"><string>serif</string></test>
    <edit name="family" mode="prepend">
      <string>Noto Serif CJK JP</string>
    </edit>
  </match>
  <match target="pattern">
    <test name="lang"><string>ja</string></test>
    <test name="family"><string>sans-serif</string></test>
    <edit name="family" mode="prepend">
      <string>Noto Sans CJK JP</string>
    </edit>
  </match>
  <match target="pattern">
    <test name="lang"><string>ja</string></test>
    <test name="family"><string>mono</string></test>
    <edit name="family" mode="prepend">
      <string>Noto Sans MONO CJK JP</string>
    </edit>
  </match>
</fontconfig>
EOL
sudo fc-cache -f
}

user_gtk(){
  [ -d "${HOME}/.config/gtk-3.0/" ] || mkdir -p ${HOME}/.config/gtk-3.0/
  cp -f /etc/gtk-2.0/gtkrc ${HOME}/.gtkrc-2.0
  sed -i -e "s/Roboto 11/${FONT}/g" ${HOME}/.gtkrc-2.0
  cp -f /etc/gtk-3.0/settings.ini ${HOME}/.config/gtk-3.0/settings.ini
  sed -i -e "s/Roboto 11/${FONT}/g" ${HOME}/.config/gtk-3.0/settings.ini
}

user_hidpi(){
  cat << EOL >> ${HOME}/.sommelierrc
# # If you're using a HiDPI chromebook you will need to adjust the dpi.
# # https://www.reddit.com/r/Crostini/comments/9g1ovl/scale_and_dpi_in_sommelierrc/
# # dpi check commands -> $ xdpyinfo | grep dots
# # example for Lenovo ideapad duet
# # ----
# echo Xft.dpi: 170 | xrdb -merge
# export GDK_SCALE=1.77
# export GDK_SCALE_DPI=170
# export SOMMELIER_SCALE=1
EOL
}

user_weston(){
  cat << EOL >> ${HOME}/.config/weston.ini
# # following are probably unnecessary. Uncomment if necessary.
# xwayland=true
# [keyboard]
# keymap_layout=jp
EOL
}

install_libreoffice(){
  mkdir -p ${HOME}/.var/app/org.libreoffice.LibreOffice/config/fontconfig
  cp /etc/fonts/local.conf ${HOME}/.var/app/org.libreoffice.LibreOffice/config/fontconfig/fonts.conf
  LANG=${USER_LOCALE} flatpak -y --noninteractive --user install org.libreoffice.LibreOffice
}

# ----

add_backports
set_locales

sudo apt -y update
sudo apt -y --no-install-recommends install debconf-i18n apt-utils locales
sudo apt -y upgrade

sudo apt -y --no-install-recommends install ${PACKAGES} # No need to install Japanese fonts

setup_inputmethod # Since we have cros-im, do we need it anymore? Ahh! it only in GTK!
sudo apt -y --no-install-recommends install avahi-daemon
install_flatpak

user_fontconfig
user_gtk
#user_hidpi
#user_weston
#install_libreoffice
sudo apt -y autoremove
sudo apt -y clean

dialog --infobox 'Linux日本語環境を使う準備ができました\nターミナルアイコンを右クリックして [Linuxをシャットダウン] からLinuxコンテナを終了してください。そして、しばらく待ってからターミナルを起動してください。\nまれに、これで反映されない場合があります。動作がおかしい場合はChromebook自体を再起動してください。\n\n*** 日本語切り替えはAlt+スペースキーです ***\nLinuxの日本語切り替えは、Chromebookの日本語切り替えキーと被らないように Alt+スペースキー に設定しています。変更する場合は、fcitx5-configtoolで設定してください。\n\nキーボードの設定を確認してください\nインプットメソッドのキー配列が実際のキー配列と違う場合があります。\nターミナルから、fcitx5-configtoolを起動して確認と変更してください。\n\nほかのLinuxアプリのインストール\nランチャーのLinuxアプリにある「ソフトウェア」(GNOME Software)を使うと、ほかのLinuxアプリもインストールできます。ご利用ください。' 0 0
